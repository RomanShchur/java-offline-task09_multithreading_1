package fibonacciNubbersThread.model;

public class FibonacciThread extends Thread {
  int count;
  public FibonacciThread() {
    this.count = 20;
  }
  @Override
  public void run() {
    int n1 = 0, n2 = 1, n3, i;
    System.out.print(n1 + " " + n2);
    for (i = 2; i < count; ++i) {
      n3 = n1 + n2;
      System.out.print(" " + n3);
      n1 = n2;
      n2 = n3;
    }
    System.out.println();
  }
}
