package fibonacciNubbersThread.controller;

import fibonacciNubbersThread.model.FibonacciThread;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class FibonacciExecClass {
  public static void main(String[] args) {
    int ThreadCount = 20;
    ExecutorService service = Executors.newCachedThreadPool();
    for (int i = 0; i < ThreadCount; i++) {
      service.submit(new FibonacciThread());
    }
  }
}
