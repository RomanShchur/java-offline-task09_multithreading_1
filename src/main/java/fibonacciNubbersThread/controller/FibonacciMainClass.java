package fibonacciNubbersThread.controller;

import fibonacciNubbersThread.model.FibonacciThread;

public class FibonacciMainClass {
  public static void main(String[] args) {
    int ThreadCount = 20;
    FibonacciThread[] threads = new FibonacciThread[ThreadCount];
    for (int i = 0; i < threads.length; i++) {
      threads[i] = new FibonacciThread();
      threads[i].start();
    }
  }
}
