package synchonizedThreads.controller;

import fibonacciNubbersThread.model.FibonacciThread;
import sleepersThreads.model.SleeperThread;
import synchonizedThreads.model.SynchronizedThreadsClass;
import synchonizedThreads.model.UnSyncronyzedThreads;

import java.util.Arrays;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadLocalRandom;

public class SynchronizedThreadsMain {
  public static void main(String[] args) {
    int ThreadCount = 5;
    SynchronizedThreadsClass[] threads = new SynchronizedThreadsClass[ThreadCount];
    for (int i = 0; i < threads.length; i++) {
      threads[i] = new SynchronizedThreadsClass();
      threads[i].start();
    }

    UnSyncronyzedThreads[] threadsUnsync =
      new UnSyncronyzedThreads[ThreadCount];
    for (int i = 0; i < threads.length; i++) {
      threadsUnsync[i] = new UnSyncronyzedThreads();
      threadsUnsync[i].start();
    }
  }
}
