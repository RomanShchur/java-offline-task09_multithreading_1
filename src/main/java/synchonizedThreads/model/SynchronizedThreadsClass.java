package synchonizedThreads.model;

import java.util.Arrays;
import java.util.concurrent.ThreadLocalRandom;

public class SynchronizedThreadsClass extends Thread {
  final SynchonizatorClass sClass = new SynchonizatorClass();
  @Override
  public void run() {
    synchronized (sClass) {
      int[] arrayToSort = new int[30];
      for (int i = 0; i < arrayToSort.length; i++) {
        arrayToSort[i] = ThreadLocalRandom.current().nextInt(0, 100);
      }
      Arrays.sort(arrayToSort);
      for (int element : arrayToSort) {
        System.out.print(element + " ");
      }
      System.out.println();
    }
  }
}
