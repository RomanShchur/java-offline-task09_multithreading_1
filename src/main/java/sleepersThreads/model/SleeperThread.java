package sleepersThreads.model;

import java.util.concurrent.ThreadLocalRandom;

public class SleeperThread extends Thread {
  @Override
  public void run() {
    long startTime = System.currentTimeMillis();
    int sleepTime = ThreadLocalRandom.current().nextInt(1000, 10000);
    try {
      Thread.sleep(sleepTime);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    long endTime = System.currentTimeMillis();
    long delta = endTime - startTime;
    System.out.println("Thread sleep =\t" + delta);
  }
}
