package sleepersThreads.controller;

import sleepersThreads.model.SleeperThread;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class SleepersExecClass {
  public static void main(String[] args) {
    int ThreadCount = 20;
    ExecutorService service = Executors.newScheduledThreadPool(5);
    for (int i = 0; i < ThreadCount; i++) {
      service.submit(new SleeperThread());
    }
    service.shutdown();
  }
}
