package fibonacciCallable.model;

import java.util.concurrent.*;

public class FibonacciCallable implements  Callable<Integer> {
  int count = 20;
  /*public FibonacciCallable() {
    this.count = 20;
  }*/
  @Override
  public Integer call() throws Exception {
    int summ = 0;
    int n1 = 0, n2 = 1, n3, i;
    System.out.print(n1 + " " + n2);
    for (i = 2; i < count; ++i) {
      n3 = n1 + n2;
      summ += n3;
      System.out.print(" " + n3);
      n1 = n2;
      n2 = n3;
    }
    System.out.println();
    System.out.println(summ);
    return summ;
  }
}
