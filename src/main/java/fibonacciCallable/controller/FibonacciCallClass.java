package fibonacciCallable.controller;

import fibonacciCallable.model.FibonacciCallable;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

public class FibonacciCallClass {
  public static void main(String[] args) {
    ExecutorService executor = Executors.newFixedThreadPool(10);
    List<Future<Integer>> list = new ArrayList<Future<Integer>>();
    Callable<Integer> callable = new FibonacciCallable();
    for(int i=0; i< 100; i++){
      Future<Integer> future = executor.submit(callable);
      list.add(future);
    }
    for(Future<Integer> fut : list){
      try {
        System.out.println(fut.get());
      } catch (InterruptedException | ExecutionException e) {
        e.printStackTrace();
      }
    }
    executor.shutdown();
  }
}
