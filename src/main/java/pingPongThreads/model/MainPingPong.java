package pingPongThreads.model;

import java.util.Random;
import java.util.concurrent.BlockingQueue;

public class MainPingPong extends Thread {
  private String name;
  private int numberOfRepeats;
  private BlockingQueue<Integer> serving;
  private BlockingQueue<Integer> receiving;

  public MainPingPong(String name,
    int numberOfRepeats,
    BlockingQueue<Integer> serving,
    BlockingQueue<Integer> receiving) {
    this.name = name;
    this.numberOfRepeats = numberOfRepeats;
    this.serving = serving;
    this.receiving = receiving;
  }

  @Override
  public void run() {
    for (int i = 0; i < numberOfRepeats; ++i) {
      try {
        int k = receiving.take();
        System.out.println(name + ": \t " + k);
        serving.put(k + 1);
      } catch (InterruptedException e) {
        break;
      }
    }
  }
}
