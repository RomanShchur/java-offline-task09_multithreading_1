package pingPongThreads.controller;

import pingPongThreads.model.MainPingPong;
import java.util.concurrent.*;

public class PingPongMainClass {
  public static void main(String[] args) throws InterruptedException {
    int RepeatNumber = 10;
    BlockingQueue<Integer> q1 = new ArrayBlockingQueue<>(1);
    BlockingQueue<Integer> q2 = new ArrayBlockingQueue<>(1);

    q2.offer(1);

    MainPingPong ping = new MainPingPong("pIng", RepeatNumber, q1, q2);
    MainPingPong pong = new MainPingPong("pOng", RepeatNumber, q2, q1);

    ping.start();
    pong.start();

    ping.join();
    pong.join();
  }
}
